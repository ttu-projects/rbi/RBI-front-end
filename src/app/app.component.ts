import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'RBI-front-end';
  menuOpen = true;
  showGraph;

  subscription: Subscription;

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    this.subscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd && event.url.indexOf('conference/') >= 0) {
        this.showGraph = true;
      } else {
        this.showGraph = false;
      }

    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  toggleMenu = () => this.menuOpen = !this.menuOpen;

  goToHome = () => this.router.navigateByUrl('/main');
}
