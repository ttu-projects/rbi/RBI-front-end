import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConferenceGraphComponent } from './conference-graph.component';

describe('ConferenceGraphComponent', () => {
  let component: ConferenceGraphComponent;
  let fixture: ComponentFixture<ConferenceGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConferenceGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConferenceGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
