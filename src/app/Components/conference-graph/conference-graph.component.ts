import { Component, Input, OnInit } from '@angular/core';
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { BubbleChartComponent, BubbleSeriesComponent } from '@swimlane/ngx-charts';

import { Conference, Player } from '../../Services/types';
import { Router } from '@angular/router';

interface Data {
  name: string;
  series: [{
    name: string;
    x: string;
    y: number;
    r: number;
  }];
}

@Component({
  selector: 'app-conference-graph',
  templateUrl: './conference-graph.component.html',
  styleUrls: ['./conference-graph.component.scss']
})
export class ConferenceGraphComponent implements OnInit {
  @Input() conference: Conference;
  loading = true;
  data: Array<Data> = [];

  constructor(private apollo: Apollo, private router: Router) {
  }

  async ngOnInit() {
    for (const team of this.conference.teams) {
      // @ts-ignore
      team.players = await this.getPlayers(team);

      for (const player of team.players) {
        const playerData: Data = {
          name: player.name,
          series: [{
            name: team.name,
            x: team.university_name,
            y: player.rbi,
            r: player.rbi
          }]
        };

        this.data.push(playerData);
      }
    }
    this.loading = false;
  }

  clickHandler(e) {
    const player = this.conference.teams.filter(team => team.university_name === e.name)[0].players.filter(p => p.name === e.series);
    this.router.navigateByUrl(`player/${player[0].id}`);
  }

  async getPlayers(team) {
    return new Promise(resolve => {
      const subscription = this.apollo.watchQuery<any>({
        query: gql`
          {
            teamPlayers(id: ${team.id}) {
            id
            name
            player_img
            class
            ht_wt
            home_town
            dob
            rbi
          }
          }
        `
      }).valueChanges.subscribe(result => {
        resolve(result.data.teamPlayers);
        subscription.unsubscribe();
      });
    });
  }
}
